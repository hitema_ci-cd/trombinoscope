import React, {useState} from "react";
import {IPerson} from "../interfaces/IPerson";
import PersonDetails from "./PersonDetails";

interface PersonPreviewProps {
    person: IPerson;
}

const PersonPreview: React.FC<PersonPreviewProps> = ({person}) => {
    const [showPopup, setShowPopup] = useState(false);

    const togglePopup = () => {
        setShowPopup(!showPopup);
    };

    return (
        <div className="person-preview" onClick={togglePopup}>
            <div className="person-preview_picture">
                <img src={person.picture} alt={person.firstName}/>
            </div>
            <div className="person-preview_info">
                <h2>{person.firstName} {person.lastName}</h2>
                <p>{person.function}</p>
            </div>

            {
                showPopup && <PersonDetails person={person} showPopup={showPopup} togglePopup={togglePopup}/>
            }

            <div className={`overlay ${showPopup ? 'active' : ''}`} onClick={togglePopup}></div>
        </div>
    );
}

export default PersonPreview;