import React from "react";
import {IPerson} from "../interfaces/IPerson";
import {PersonsContext} from "../contexts/personsContext";

interface PersonDetailsProps {
    person: IPerson;
    showPopup: boolean;
    togglePopup: () => void;
}

const PersonDetails: React.FC<PersonDetailsProps> = ({person, showPopup, togglePopup}) => {
    const {dispatchPersons} = React.useContext(PersonsContext);
    const [personDetails, setPersonDetails] = React.useState<IPerson>(person);

    const handleChanges = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;

        setPersonDetails({...personDetails, [name]: value})
    }

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        dispatchPersons({type: 'UPDATE_PERSON', payload: personDetails});
        togglePopup();
    }

    const handleDelete = () => {
        dispatchPersons({type: 'DELETE_PERSON', payload: personDetails.id});
        togglePopup();
    }

    return (
        <div className={`person-details popup ${showPopup ? 'active' : ''}`} onClick={(event) => event.stopPropagation()}>
            <form onSubmit={handleSubmit}>
                <p>
                    <img src={personDetails.picture} alt={personDetails.firstName + '' + personDetails.lastName}/>
                    {/*<input type="image"/>*/}
                </p>
                <p>
                    <label htmlFor="firstName">Prénom</label>
                    <input type="text" name="firstName" value={personDetails.firstName} onChange={handleChanges}/>
                </p>
                <p>
                    <label htmlFor="lastName">Nom</label>
                    <input type="text" name="lastName" value={personDetails.lastName} onChange={handleChanges}/>
                </p>
                <p>
                    <label htmlFor="function">Fonction</label>
                    <input type="text" name="function" value={personDetails.function} onChange={handleChanges}/>
                </p>
                <p>
                    <label htmlFor="email">Email</label>
                    <input type="text" name="email" value={personDetails.email} onChange={handleChanges}/>
                </p>
                <p>
                    <label htmlFor="telephone">Téléphone</label>
                    <input type="text" name="telephone" value={personDetails.telephone} onChange={handleChanges}/>
                </p>
                <p>
                    <label htmlFor="address">Adresse</label>
                    <input type="text" name="address" value={personDetails.address} onChange={handleChanges}/>
                </p>
                <button type="submit" data-button="submit">Enregistrer</button>
                <button type="button" data-button="cancel" onClick={togglePopup}>Annuler</button>
                <button type="button" data-button="delete" onClick={handleDelete}>Supprimer</button>
            </form>
        </div>
    );
}

export default PersonDetails;