import React from 'react';
import logo from './logo.svg';
import './App.css';
import {PersonsContext} from "./contexts/personsContext";
import PersonPreview from "./components/PersonPreview";

function App() {
    const {persons} = React.useContext(PersonsContext);

    return (
        <div className="App">
            <header className="App-header">
                <div className="header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1>Trombinoscope</h1>
                </div>
                <div className="persons-list">
                    {
                        persons.map((person, index) => <PersonPreview key={index} person={person}/>)
                    }
                </div>
            </header>
        </div>
    );
}

export default App;
