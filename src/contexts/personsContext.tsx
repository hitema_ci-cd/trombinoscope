import {createContext, Dispatch, PropsWithChildren, Reducer, useReducer} from "react";
import {IPerson} from "../interfaces/IPerson";

interface IPersonsContext {
    persons: IPerson[];
    dispatchPersons: Dispatch<any>;
}

export const PersonsContext = createContext<IPersonsContext>({
    persons: [],
    dispatchPersons: () => {}
});

const PersonsReducer: Reducer<IPerson[], any> = (state, action) => {
    switch (action.type) {
        case 'ADD_PERSON':
            return [...state, action.payload];

        case 'DELETE_PERSON':
            return state.filter((person: IPerson) => person.id !== action.payload);

        case 'UPDATE_PERSON':
            return state.map((person: IPerson) => person.id === action.payload.id ? action.payload : person);

        default:
            return state;
    }
}

export const PersonsContextProvider = (props: PropsWithChildren<{}>) => {
    const initialState: {trombinoscope: IPerson[]} = require('../data/db.json');
    const [persons, dispatchPersons] = useReducer(PersonsReducer, initialState.trombinoscope);

    return (
        <PersonsContext.Provider value={{persons, dispatchPersons}}>
            {props.children}
        </PersonsContext.Provider>
    );
}