export interface IPerson {
    id: string;
    firstName: string;
    lastName: string;
    function: string;
    email: string;
    telephone: string;
    address: string;
    picture: string;
}